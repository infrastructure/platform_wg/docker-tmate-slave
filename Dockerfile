FROM alpine:edge AS base
MAINTAINER Varac <varac@varac.net>

RUN apk update && \
    apk upgrade && \
    apk add --no-cache \
        libc6-compat \
        msgpack-c \
        ncurses-libs \
        libevent \
        openssh-keygen


FROM base AS build

ADD backtrace.patch /

RUN apk add --no-cache --virtual build-dependencies \
        build-base \
        ca-certificates \
        bash \
        wget \
        git \
        automake \
        autoconf \
        zlib-dev \
        libevent-dev \
        msgpack-c-dev \
        ncurses-dev \
        libexecinfo-dev \
        cmake \
        openssl-dev \
        libgcrypt-dev \
        mbedtls-dev && \
    mkdir /src && \
    cd /src && \
    wget https://www.libssh.org/files/0.8/libssh-0.8.7.tar.xz && \
    tar Jxf libssh-0.8.7.tar.xz && \
    cd libssh-0.8.7 && \
    mkdir build && cd build && \
    cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release .. && \
    make -j && \
    mkdir /libssh && make install DESTDIR=/libssh && make install && \
    git clone https://github.com/tmate-io/tmate-ssh-server.git /src/tmate-server && \
    cd /src/tmate-server && \
    # after this commit (Dec 2018) support for ed25519 host keys was
    # introduced, but it it's not working currently
    # (see https://github.com/tmate-io/tmate-ssh-server/issues/54,
    #  "Cannot authenticate when server has ed25519 keys").
    git checkout e4037b703c16cd34677dc4c90ce611b60a854393 && \
    git apply /*.patch && \
    ./autogen.sh && \
    ./configure CFLAGS="-D_GNU_SOURCE" && \
    make -j


FROM base AS final

ADD tmate-server.sh /tmate-server.sh
ADD create_keys.sh /bin

COPY --from=build /libssh/* /
COPY --from=build /src/tmate-server/tmate-slave /bin/tmate-server

CMD ["/tmate-server.sh"]
